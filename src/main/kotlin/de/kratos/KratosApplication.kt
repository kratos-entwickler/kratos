package de.kratos

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KratosApplication

fun main(args: Array<String>) {
    runApplication<KratosApplication>(*args)
}
